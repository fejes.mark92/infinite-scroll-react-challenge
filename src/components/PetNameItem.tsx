import './PetNameItem.css';

export interface PetNameProps {
  name: string;
}

export function PetNameItem({ name }: PetNameProps) {
  return <div className="pet-name-item-container">{name}</div>;
}
