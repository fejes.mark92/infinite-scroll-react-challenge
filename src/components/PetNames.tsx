import _ from 'lodash';
import { useContext, useEffect, useMemo, useRef, useState } from 'react';
import { PetNamesActionType } from '../store/pet-names.actions';
import { PetNamesContext } from '../store/pet-names.context';
import { PetNameItem } from './PetNameItem';
import './PetNames.css';

export function PetNames() {
  const [markerElement, setMarkerElement] = useState<HTMLDivElement | null>(null);
  const [isLoading, setIsLoading] = useState(false);
  const { petNamesState, petNamesDispatch } = useContext(PetNamesContext);

  const intersectionObserver = useRef(
    new IntersectionObserver((entries) => {
      const first = entries[0];
      if (first.isIntersecting && !isLoading) {
        setPetNameIdOffset();
      }
    }),
  );

  useEffect(() => {
    // always observe the current marker
    const currentElement = markerElement;
    const currentObserver = intersectionObserver.current;

    if (currentElement) {
      currentObserver.observe(currentElement);
    }

    return () => {
      if (currentElement) {
        currentObserver.unobserve(currentElement);
      }
    };
  }, [markerElement]);

  useEffect(() => {
    const lastPetnameId = getLastPetNameId();
    if (lastPetnameId !== petNamesState?.petNameOffsetId) {
      setIsLoading(false);
    }
  }, [petNamesState]);

  useEffect(() => {
    if (!petNamesState?.hasMoreItems) {
      setIsLoading(false);
    }
  }, [petNamesState]);

  const petArray = useMemo(() => {
    return _.values(petNamesState?.petNames);
  }, [petNamesState]);

  const canLoadMore = useMemo(() => {
    return (
      !isLoading && petNamesState?.hasMoreItems && Object.keys(petNamesState.petNames).length > 0
    );
  }, [petNamesState, isLoading]);

  function setPetNameIdOffset() {
    const petNameOffsetId = getLastPetNameId();
    setIsLoading(true);
    petNamesDispatch!({
      type: PetNamesActionType.SetPetNameOffsetId,
      petNameOffsetId: petNameOffsetId,
    });
  }

  function getLastPetNameId() {
    const sortedPetNames = _.sortBy(petNamesState?.petNames, (petName) => petName.id);
    const lastPetId =
      sortedPetNames.length > 0 ? sortedPetNames[sortedPetNames.length - 1].id : null;
    return lastPetId;
  }

  return (
    <>
      <div className="pet-names-container">
        {petArray.map((pet) => (
          <PetNameItem key={pet.id} name={pet.name}></PetNameItem>
        ))}
      </div>
      {canLoadMore ? <div ref={setMarkerElement} id="intersection-marker"></div> : null}
      {isLoading ? <div ref={setMarkerElement} id="loading-marker"></div> : null}
    </>
  );
}
