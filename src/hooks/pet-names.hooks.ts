import { useEffect, useReducer, useState } from 'react';
import { PetType } from '../models/pet-name-item';
import { PetNamesActions } from '../store/pet-names.actions';
import { petNamesInitialState, petNamesReducer, PetNamesState } from '../store/pet-names.reducer';

export function usePetNamesReducer() {
  return useReducer<React.Reducer<PetNamesState, PetNamesActions>>(
    petNamesReducer,
    petNamesInitialState,
  );
}

export function usePetNamesListingParams(petNamesState: PetNamesState) {
  const [petNamesListingParams, setPetNamesListingParams] = useState<{
    petNamesOffsetId: string | null;
    petType: PetType;
  }>({
    petNamesOffsetId: null,
    petType: PetType.All,
  });

  useEffect(() => {
    // Check if any parameter changed for listing
    if (
      petNamesState.petNameOffsetId !== petNamesListingParams.petNamesOffsetId ||
      petNamesState.petType !== petNamesListingParams.petType
    ) {
      setPetNamesListingParams({
        petNamesOffsetId: petNamesState.petNameOffsetId,
        petType: petNamesState.petType,
      });
    }
  }, [petNamesState]);

  return petNamesListingParams;
}
