import { useEffect } from 'react';
import { loadPetNamesApi } from './api/pet-name-api';
import './App.css';
import { PetNames } from './components/PetNames';
import { usePetNamesListingParams, usePetNamesReducer } from './hooks/pet-names.hooks';
import { PetNameItem, PetType } from './models/pet-name-item';
import { PetNamesActionType } from './store/pet-names.actions';
import { PetNamesContextProvider } from './store/pet-names.context';

function App() {
  const [petNamesState, petNamesDispatch] = usePetNamesReducer();
  const petNamesListingParams = usePetNamesListingParams(petNamesState);

  useEffect(() => {
    async function loadPetNames() {
      const petNamesReponse = (await loadPetNamesApi({
        offsetPetNameId: petNamesListingParams.petNamesOffsetId,
        petType: petNamesListingParams.petType,
        limit: 20,
      })) as { [key: string]: PetNameItem };
      petNamesDispatch({
        type: PetNamesActionType.AddPetNames,
        petNames: petNamesReponse,
      });

      if (Object.keys(petNamesReponse).length === 0) {
        petNamesDispatch({
          type: PetNamesActionType.SetHasMoreItems,
          hasMoreItems: false,
        });
      }
    }
    loadPetNames();
  }, [petNamesListingParams]);

  function onPetTypeChange(event: any) {
    petNamesDispatch({
      type: PetNamesActionType.SetPetType,
      petType: event.target.value,
    });
  }

  return (
    <PetNamesContextProvider value={{ petNamesState, petNamesDispatch }}>
      <div className="container">
        <h1>Pet names</h1>
        <select
          value={petNamesState.petType}
          onChange={(choice) => onPetTypeChange(choice)}
          name="pets"
          id="pet-select">
          <option value={PetType.All}>--Please choose an option--</option>
          <option value={PetType.Dog}>Dog</option>
          <option value={PetType.Cat}>Cat</option>
        </select>
        <div className="container">
          <PetNames></PetNames>
        </div>
      </div>
    </PetNamesContextProvider>
  );
}

export default App;
