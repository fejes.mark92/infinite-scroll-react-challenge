import { PetNameItem, PetType } from '../models/pet-name-item';

export enum PetNamesActionType {
  AddPetNames = 'AddPetNames',
  SetPetNameOffsetId = 'SetPetNameOffsetId',
  SetPetType = 'SetPetType',
  SetHasMoreItems = 'SetHasMoreItems',
}

export type PetNamesActions =
  | { type: PetNamesActionType.AddPetNames; petNames: { [key: string]: PetNameItem } }
  | { type: PetNamesActionType.SetPetNameOffsetId; petNameOffsetId: string | null }
  | { type: PetNamesActionType.SetPetType; petType: PetType }
  | { type: PetNamesActionType.SetHasMoreItems; hasMoreItems: boolean };
