import { createContext } from 'react';
import { PetNamesActions } from './pet-names.actions';
import { PetNamesState } from './pet-names.reducer';

const petNameContext: {
  petNamesState: PetNamesState | null;
  petNamesDispatch: ((action: PetNamesActions) => void) | null;
} = {
  petNamesState: null,
  petNamesDispatch: null,
};
export const PetNamesContext = createContext(petNameContext);
export const PetNamesContextProvider = PetNamesContext.Provider;
export const PetNamesContextConsumer = PetNamesContext.Consumer;
