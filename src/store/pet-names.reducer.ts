import { PetNameItem, PetType } from '../models/pet-name-item';
import { PetNamesActions, PetNamesActionType } from './pet-names.actions';
import _ from 'lodash';

export interface PetNamesState {
  petNames: { [key: string]: PetNameItem };
  petNameOffsetId: string | null;
  petType: PetType;
  hasMoreItems: boolean;
}

export const petNamesInitialState: PetNamesState = {
  petNames: {},
  petNameOffsetId: null,
  petType: PetType.All,
  hasMoreItems: true,
};

export function petNamesReducer(state: PetNamesState, action: PetNamesActions) {
  switch (action.type) {
    case PetNamesActionType.AddPetNames: {
      return { ...state, petNames: _.merge(state.petNames, action.petNames) };
    }
    case PetNamesActionType.SetPetNameOffsetId: {
      return { ...state, petNameOffsetId: action.petNameOffsetId };
    }
    case PetNamesActionType.SetPetType: {
      // reset petNames and offset
      return {
        ...state,
        petType: action.petType,
        petNames: {},
        petNameOffsetId: null,
        hasMoreItems: true,
      };
    }
    case PetNamesActionType.SetHasMoreItems: {
      return { ...state, hasMoreItems: action.hasMoreItems };
    }
  }
}
