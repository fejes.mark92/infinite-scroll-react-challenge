import _ from 'lodash';
import { PetType } from '../models/pet-name-item';
import { catNames, dogNames } from './pet-names-data';

const catNamesWithType = [...catNames].map((name) => ({
  name,
  type: PetType.Cat,
}));

const dogNamesWithType = [...dogNames].map((name) => ({
  name,
  type: PetType.Dog,
}));

const petNames = _.sortBy([...catNamesWithType, ...dogNamesWithType], ['name']).map(
  ({ name, type }, index) => ({
    name,
    type,
    id: index,
  }),
);

export async function loadPetNamesApi({
  offsetPetNameId,
  limit,
  petType,
}: {
  offsetPetNameId: string | null;
  limit: number;
  petType: PetType;
}) {
  return new Promise((resolve) => {
    setTimeout(() => {
      let startIndex = 0;
      const filteredPetNames = petNames.filter(
        (petItem) => petItem.type === petType || petType === PetType.All,
      );

      if (offsetPetNameId !== null) {
        const parsedOffset = Number.parseInt(offsetPetNameId, 10);
        startIndex = filteredPetNames.findIndex((petName) => petName.id > parsedOffset);
      }

      const returnPetObject = filteredPetNames
        .slice(startIndex, limit + startIndex)
        .reduce((acc: any, current) => {
          acc[current.id.toString()] = {
            id: current.id,
            name: current.name,
            type: current.type,
          };
          return acc;
        }, {});

      resolve(returnPetObject);
    }, 500);
  });
}
