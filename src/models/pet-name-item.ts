export interface PetNameItem {
  id: string;
  name: string;
}

export enum PetType {
  All = 'All',
  Dog = 'Dog',
  Cat = 'Cat',
}
